"""
Tensorflow implementation of a Self-Organizing Map algorithm. 
Specifically created to be suited for multiple runs, i.e. initialization of the graph component is independent of the training operations.
This code allows the user to visualize the SOM in 3D, the third dimension is not used in training but can be used for labeling the samples,
e.g. to create an evolutionary fitness landscape (see Lorenzo-Redondo et al. 2014)
For the purpose of creating these landscapes, there is an option to draw arrows
between networks mapped on the SOM, based on a weighted sum of the mutational
distance and fitness gain. 
To check the quality of a trained som the check_quality() function can be used, which returns two values:
    The correlation between the LD-distance matrix and HD-distance matrix
    The 'stress' between the LD and HD mapping.
    
Additionally, this code also features a SOM-clustering algorithm as well as a functions which forms a phylogenetic tree from said clustering. These trees can then be used
to form a consensus tree. All SOMs used to create these trees can easily be saved and thus loaded in later on to visualize the SOM that best represents a given consensus tree.
The consensus tree algorithm is provided separately in an .R file, but the generation of the consensus trees is automated via a subprocess call.

@author: Pim Fuchs
"""

#Plotting
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import animation
from matplotlib import *

from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
from matplotlib.widgets import Slider, Button, RadioButtons


import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'



import numpy as np
import subprocess

sys.setrecursionlimit(2500) # clustering of large maps requires high recursion

class SOM:
    
    #==============================LOADING AND SAVING===========================================
    
    def save(self, filename):
        """Save the SOM
        
        filename:   string indicating the location where the SOM should be saved
        """
        create_missing_dirs(filename) # Create the missing dirs
        np.save('%s_bmu'%(filename), self.bmus)
        np.save('%s_codebook'%(filename), self.codebook)
        try: # Check if there are clusters to save (for the tree)
            self.clustrs
            with open('%s_tree.tree' %(filename),'w') as outp:
                outp.write(self.tree)
            with open('%s_clusters.txt' %(filename),'w') as outp:
                outp.write(str(self.clustrs))
        except AttributeError:
            pass
        
    def load(self, filename):
        """Load a previously saved som
        
        filename:   string, same as used to save the file
        """
        self.bmus = np.load('%s_bmu.npy' %(filename))
        self.bmu_uni = np.ascontiguousarray(self.bmus).view(np.dtype((np.void, self.bmus.dtype.itemsize * self.bmus.shape[1])))
        _, indx = np.unique(self.bmu_uni, return_index=True)
        self.bmu_uni = indx
        self.codebook = np.load('%s_codebook.npy' %(filename))
        self.n_rows = self.codebook.shape[0]
        self.n_columns = self.codebook.shape[1]
        self.n_vars = self.codebook.shape[-1]
        self.n_samples = len(self.bmus)
        try:
            self.clustrs = eval(open('%s_clusters.txt' %(filename)).read())
            self.tree = open('%s_tree.tree' %(filename)).read()
        except:
            print('No tree/clusters are loaded, if they are saved then something went wrong...')
    
    
    #==============================MAIN SOM ALGORITHM===========================================
    def __del__(self):
        try:
            # Otherwise nodes of the previous instance will accumulate in the graph
            tf.reset_default_graph() 
            self.sess.close()
        except:
            pass
            
    def __init__(self, data=None,dim=None, load_file=None, init='random', gridtype='hexagonal',
    epochs = 1000, initial_lr = 0.1, initial_radius = None, toroid=True):   
        """Initialize the Self-Organizing Map,
        
        data:       Numpy array containing the samples on the rows and the 
                    features on the columns
        dim:        list of desired SOM dimensions, i.e. [rows, columns]
        load_file:  filename of a previously saved SOM. Data and dim arguments
                    are not required if a load_file is used.
        init:       Specifies how the initial map values are determined, valid
                    options are: 'random','pca'
        gridtype:   The type of cell that is used, valid options are: 
                    'rectangular', 'hexagonal'
        epochs:     Integer value indicating the no. of update steps to perform 
                    before stopping.
        initial_lr: Float value indicating the learning rate at the beginning
                    of the algorithm. The decay of the learning rate is:
                    initial_lr * exp(-epoch/tot_epoch)
        toroid:     Boolean value indicating whether the map is a torus or not,
                    i.e. whether the neighbourhoods wrap around to the other
                    side of the map.
        initial_radius: Float value, the neighbourhood radius at the beginning 
                        of the algorithm. If no value is given it defaults to
                        0.5*min(map_row, map_col). The decay of the radius is:
                        initial_radius*exp(-epoch/(epochs/log(initial_radius)))
        """

        self.toroid = toroid
        if load_file:
            print('Loading SOM from file...')
            self.gridtype = gridtype
            self.data = data
            self.load(load_file)
        else: 
            try:
                self.n_rows = dim[0]
                self.n_columns = dim[1]
                self.n_vars = data.shape[-1] # No. of variables
            except TypeError:
                print('Error: Provide data and map dimensions as data and dim arguments respectively. Alternatively you can provide a load_file argument to load a previously generated SOM.')
                return(None)
                
            # Only import TensorFlow when we need it
            # Since the class can also be used to just visualize
            import tensorflow as tf
                        
            self.n_samples = len(data)
            self.getbmus = tf.Variable(False, tf.bool) #False when training ,true when requesting the BMUs after training --> False makes sure that the samples are generated in a shuffled order during training
            self.bmu_q = tf.FIFOQueue(self.n_samples,'int32')
            self.q = tf.RandomShuffleQueue(self.n_samples,1,'int32')
            self.current_sample = tf.cond(self.getbmus, self.bmu_q.dequeue, self.q.dequeue)
            
            #self.data = data
            self.__data = tf.constant(data, name='data')
            self.sample_matrix = tf.cast(self.__data[self.current_sample], dtype='float64')

            self.__codebook = tf.Variable(tf.cast(tf.random_normal([self.n_rows,self.n_columns, self.n_vars]),dtype='float64'),dtype='float64', name='codebook')#, #, mean= np.mean(data), stddev= np.std(data))) #random initialization
                        
            self.raw_dist = self.sample_matrix-self.__codebook #Raw distance between components/variables

            min_arg = tf.argmin(tf.reshape(tf.sqrt(tf.reduce_sum(tf.pow(self.raw_dist,2),len(self.__data.shape))),[-1]),axis=0) #Minimal entry in the flattened distance matrix
            self.bmu_index = [tf.cast(min_arg/self.n_columns,'int32'),tf.cast(tf.mod(min_arg,self.n_columns),'int32')] #Calculate the original 2D indices from the flattened index
            
            # For a hexagonal grid the rule is as follows:
            # For cells with an even row 2R,C the columns of the above and below neighbours are C-1 and C
            # For uneven cell rows this is C and C+1
            # Hence the neighbours could be determined as C-1*(R%2) and C+1-1*(R%2)
            self.gridtype = gridtype #Important for clustering later on
            if self.gridtype == 'hexagonal':
                # Determine the location of the hexagonal centers
                self.centers = tf.constant(np.array([[[r,c+(0.5*(r%2))] for c in range(self.n_columns)] for r in range(self.n_rows)]),dtype='float32', name='center_matrix') #Hexagonal Cells
                # Find the shortest path to each cell, i.e. to wrap or not to wrap (torus)
                if self.toroid == True:
                    minny = tf.minimum(tf.abs(tf.subtract(self.centers,self.centers[self.bmu_index])), np.array([self.n_rows, self.n_columns]) - tf.abs(tf.subtract(self.centers,self.centers[self.bmu_index])))
                else:
                    minny = tf.subtract(self.centers,self.centers[self.bmu_index])
                cell_distances = tf.sqrt(tf.reduce_sum(tf.pow(tf.cast(minny,dtype='float64'),2),len(self.__data.shape))) #Distance from the BMU index to all other indices
            elif self.gridtype == 'rectangular':
                self.map_coordinates = tf.constant(np.array([[[r,c] for c in range(self.n_columns)] for r in range(self.n_rows)]),dtype='int32', name='index_matrix') #Rectangular Cells
                if self.toroid==True:
                    # Find the shortest path to each cell, i.e. to wrap or not to wrap (torus)
                    minny = tf.minimum(tf.abs(tf.subtract(self.map_coordinates,self.bmu_index)), np.array([self.n_rows, self.n_columns]) - tf.abs(tf.subtract(self.map_coordinates,self.bmu_index)))
                else:
                    minny = tf.subtract(self.map_coordinates,self.bmu_index)
                
                # Matrix of vectors where each vector is of the format [i,j] where i is the row and j is the column of the entry
                cell_distances = tf.sqrt(tf.reduce_sum(tf.pow(tf.cast(minny,dtype='float64'),2),len(self.__data.shape))) #Distance from the BMU index to all other indices
            
            if not initial_radius:
                initial_radius = tf.minimum(self.n_rows,self.n_columns)/2
            else:
                initial_radius = tf.Variable(initial_radius,dtype='float64')
    
            self.tot_epochs = epochs
            
            self.epochs = tf.Variable(epochs ,dtype='float64',name='epochs')
            initial_radius = tf.Variable(initial_radius, dtype='float64', name='intial_radius')
            
            self.random_init = tf.assign(self.__codebook, tf.cast(tf.random_normal([self.n_rows,self.n_columns, self.n_vars], mean= np.mean(data), stddev= np.std(data)),dtype= tf.float64)) #random initialization
            self.initial_lr = tf.Variable(initial_lr,dtype='float64', name='initial_lr') #Learning rate at the start of training        
            
            self.epoch_no = tf.Variable(0.0,dtype='float64', name='epoch_no') #Current epoch no
            
            #self.bmu_q.enqueue_many(([x for x in range(self.n_samples)],))
            self.restart = [self.epoch_no.assign(0)] #Reset the epoch number in case this SOM was previously trained
            #self.tc = self.epochs/tf.log(self.initial_radius) #time constant
            radius = initial_radius*tf.exp(-self.epoch_no/(self.epochs/tf.log(initial_radius))) #neighbourhood function
            learning_rate = self.initial_lr*tf.exp(-self.epoch_no/self.epochs) #Calculate learning rate
            
            # Identify the 'neighbours', i.e. the cells on the map that are within a certain radius of the BMU
            self.neighbours = tf.cast(tf.less(cell_distances,radius),dtype='float64', name='neighbours')
            
            # Matrix of learning rates; the first term separates non-neighbours from neighbours
            # The second term is the learning_rate adjustment based on vicinity, i.e. the more distant a neighbour, the lower the LR
            # The third term is the learning rate (which decays over time, see above)
            self.lr_matrix = (self.neighbours * tf.exp(-(cell_distances/radius)) * learning_rate)
            
            #self.delta_weight = tf.stack([self.lr_matrix]*self.n_vars,axis=2) * self.raw_dist
            
            # The operations we actually run
            self.update_operation = tf.assign(self.__codebook, self.__codebook + tf.stack([self.lr_matrix]*self.n_vars,axis=2) * self.raw_dist) #Entire SOM process
            # Increase epoch_no to adjust LR and neighbourhood function
            self.incr = self.epoch_no.assign_add(1) 
            
            # Get new sample from queue
            self.incr_sample = self.q.enqueue([self.current_sample]) 
            
            init = tf.global_variables_initializer()
            self.sess = tf.Session()
            self.sess.run(init)
            
            # Make sure the samples are in the queue
            self.sess.run(self.q.enqueue_many(([x for x in range(self.n_samples)],)))
            
            # After the last update, we want the BMU requests to happen in order
            self.set_bmu_false = tf.assign(self.getbmus,False) 
            self.set_bmu_true = tf.assign(self.getbmus,True)
            
            # The list of operations each thread will run
            self.threadop = [self.update_operation, self.incr, self.incr_sample]
            

    def run_epoch(self):
        """The function to be called for each thread, which runs the SOM update
        algorithm
        """

        #, feed_dict = {self.getbmus : False})
        # Check if we are done, if so let the coordinator know that
        # other threads should be stopped as well
        if self.sess.run(self.epoch_no) >= self.tot_epochs:
            self.coord.request_stop()
        else:
            self.sess.run(self.threadop)
            
                
    def train(self):
        """Trains the SOM, this is where the TensorFlow graph is actually run. 
        The graph construction is done in the initialization to facilitate 
        efficient re-training of a SOM on the same data. This, in turn, speeds
        up the creation of the SOM-based trees, which rely on multiple SOMs
        for majority clustering
        """
        # Only implement TensorFlow when we need it
        import tensorflow as tf
        import threading
        
        start_train = []
        
        #Make sure some parts of the graph are restarted
        self.sess.run(self.random_init)
        self.sess.run(self.restart)
        self.sess.run(self.set_bmu_false)
        
        #self.writer = tf.summary.FileWriter('./graphs', self.sess.graph) #only if we want to use the TensorBoard
        
        #Multithread training
        self.coord = tf.train.Coordinator()
        threads = [tf.train.LooperThread(self.coord, None, self.run_epoch) for i in range(8)]
        for t in threads: t.start()

        self.coord.join(threads) # Join threads when done
        
        # Identify Best-Matching Units
        bmus = []
        self.sess.run(self.set_bmu_true)
        self.sess.run(self.bmu_q.enqueue_many(([x for x in range(self.n_samples)],)))
        print("Map trained!")
        print("Identifying BMUs...")
        for sample in range(self.n_samples):
            bmus.append(self.sess.run(self.bmu_index))
    
        self.bmus = np.array([[x[1],x[0]] for x in bmus]) #N.B. inversed the axis so they are in the format [x,y] --> SO NOT [ROW,COLUMN] BUT [COLUMN, ROW]
        #Get the unique BMUs, useful for when we start plotting
        self.bmu_uni = np.ascontiguousarray(self.bmus).view(np.dtype((np.void, self.bmus.dtype.itemsize * self.bmus.shape[1])))
        _, indx = np.unique(self.bmu_uni, return_index=True)
        self.bmu_uni = indx 
        self.codebook = self.sess.run(self.__codebook)
        self.data = self.sess.run(self.__data)
        #self.writer.close 

    
    #==============================PLOTTING=====================================================
    def fitness_coords(self):
        """Creates the x,y,z components for a fitness plot."""
        x = np.array([[i for i in range(self.fit_matrix.shape[1])] for q 
                        in range(self.fit_matrix.shape[0])])
        y = np.array([[q for i in range(self.fit_matrix.shape[1])] for q 
                        in range(self.fit_matrix.shape[0])])
        z = self.fit_matrix
        return(x,y,z)
    
    
    def prep_graph(self, labels=False, arrows=False):
        """Prepares the graph for plotting
        labels:     Boolean indicating whether or not to plot labels
        arrows:     Boolean indicating whether or not to plot arrows """
        
        # Determine what to colour the cells by
        if self.plot_type == 'component':
            self.col_matrix = self.codebook[:,:,self.plane_no]
        elif self.plot_type == 'umatrix':
            self.umatrix = self.create_uma()
            self.col_matrix = self.umatrix
        elif self.plot_type == 'cluster':
            cols = [i for i in range(max(self.clustrs[0].values())+1)]
            self.col_matrix = np.array([
                    [cols[self.clustrs[self.clust_no][(row,col)]] for col 
                    in range(self.n_columns)] for row in range(self.n_rows)])
        
        self.ax = self.fig.gca(projection='3d')
        # colourmap selection
        m = plt.cm.ScalarMappable(cmap='Spectral_r') 
        m.set_array(self.col_matrix)
        cols = m.to_rgba(self.col_matrix)
        
        self.x,self.y,self.z = self.fitness_coords()
        surf = self.ax.plot_surface(self.x,self.y,self.z, facecolors = cols,
                                linewidth =0, antialiased=True, shade=False)
        # If no fitness matrix is provided
        if self.fitness.count(0) == len(self.fitness): 
            self.ax.set_zlim3d(0,1)
        else:
            self.ax.set_zlim3d(0,max(self.fitness)*1.2)
        self.fig.colorbar(m)#, shrink=0.5, aspect=5)    
        self.ax.set_axis_off()
        
        if arrows:
            self.draw_arrows(self.target_list)
        
        if labels:
            for b in self.bmu_uni:
                bm = self.bmus[b]
                self.ax.text(bm[0], bm[1], self.z[bm[1],bm[0]], b, color='black',size=7) #possible add a fraction to the x and y coordinates to center it, but don't forget to do it for the arrows as well

    def refresh_plot(self):
        """Updates the plot when a different mode is selected, i.e. component,
        U-matrix or cluster view.
        """
        azim = self.ax.azim
        elev = self.ax.elev
        
        #Get old camera position
        z_zoom = self.ax.get_zlim3d()
        x_zoom = self.ax.get_xlim3d()
        y_zoom = self.ax.get_ylim3d()
        
        self.fig.clear()
        self.prep_graph(labels=self.labels, arrows=self.arrows)
        
        #Make sure the right camera positions are retained in the next graph
        self.ax.view_init(elev=elev, azim=azim)
        self.ax.set_zlim3d(z_zoom[0],z_zoom[1])
        self.ax.set_xlim3d(x_zoom[0],x_zoom[1])
        self.ax.set_ylim3d(y_zoom[0],y_zoom[1])
    
    def change_to_component(self,mousestuff):
        """Changes the plot to component view"""
        self.plot_type = 'component'
        self.refresh_plot()
        self.draw_widgets()
        self.fig.canvas.set_window_title('Component plane %s' %(self.plane_no))
        plt.show()
        self.fig.clear()
        
    def change_to_cluster(self,mousestuff):
        """Changes the plot to cluster view"""
        try:
            self.clustrs
        except AttributeError:
            print("Clusters not calculated, running SOM.get_tree() to generate clusters...")
            self.get_tree(dont_create_newick=True)
            print('Done clustering!')
        self.plot_type = 'cluster'
        self.refresh_plot()
        self.draw_widgets()
        self.fig.canvas.set_window_title('Clustering at thresold %s' %(self.clust_no))
        plt.show()
        self.fig.clear()
    
    def change_plane(self,mousestuff):
        """Changes the component plane according to the selected plane no
        on the slider widget"""
        
        try:
            num = self.button.label.get_text()
        except:
            print('No different plane selected...')
            return()

        self.refresh_plot()
        #if arrows:
        #    self.ax = draw_arrows(som, target_list, scalar, z, ax, bmu_uni)
        if self.plot_type == 'component':
            self.fig.canvas.set_window_title('Component plane %s' %(num))
        elif self.plot_type == 'cluster':
            self.fig.canvas.set_window_title('Clustering at thresold %s' %(num))
   
        self.draw_widgets()
        plt.show()
        self.fig.clear()
        
    def update_plane_no(self,num):
        """Changes the button label to the slider value
        num:    int, the component plane number."""
        if self.plot_type == 'component':
            self.plane_no = int(num)
            self.button.label.set_text(self.plane_no)
        elif self.plot_type == 'cluster':
            self.clust_no = round(num*self.n_thresholds)/self.n_thresholds
            self.button.label.set_text(self.clust_no)
            
    def draw_widgets(self):
        """
        the_table = plt.table(cellText='test',
                      rowLabels=['row1','row2','row3','row4'],
                      colLabels=['col1','col2','col3','col4'],
                      loc='right')#, bbox=[0.25, -0.5, 0.5, 0.3])
        """
        
        ax_component_button = plt.axes([0.12,0.9,0.2,0.1])
        ax_cluster_button = plt.axes([0.35,0.9,0.2,0.1])
        self.button_component = Button(ax_component_button, 'View Component Planes', hovercolor='0.875')
        self.button_cluster = Button(ax_cluster_button, 'View Cluster Map', hovercolor='0.875')
        self.button_component.on_clicked(self.change_to_component)
        self.button_cluster.on_clicked(self.change_to_cluster)
        
        if self.plot_type == 'component':
            ax_slide = plt.axes([0.2,0.01,0.65,0.03])
            ax_but = plt.axes([0.2,0.05,0.65,0.03])
            self.cpl_slider = Slider(ax_slide, 'Component Plane', 0, self.codebook.shape[2]-1, valinit=0,valfmt='%0.0f')
            #cpl_slider_net = Slider(ax_slide_net, 'Selected network', 0, som.bmus.shape[0], valinit=0)
            self.button = Button(ax_but, 'View plane %d' %(self.plane_no), hovercolor='0.975')
            self.cpl_slider.on_changed(self.update_plane_no)
            self.button.on_clicked(self.change_plane)
        elif self.plot_type == 'cluster':
            ax_slide = plt.axes([0.2,0.01,0.65,0.03])
            #ax_slide_net = plt.axes([0.2,0.9, 0.65, 0.03])
            ax_but = plt.axes([0.2,0.05,0.65,0.03])
            self.cpl_slider = Slider(ax_slide, 'Cluster threshold', 0, max(set(self.clustrs.keys())), valinit=0, valfmt='%1.3f')
            #cpl_slider_net = Slider(ax_slide_net, 'Selected network', 0, som.bmus.shape[0], valinit=0)
        
            
            self.button = Button(ax_but, 'View clustering at threshold %.3f' %(self.clust_no), hovercolor='0.975')
            self.cpl_slider.on_changed(self.update_plane_no)
            self.button.on_clicked(self.change_plane)
            
    def plot(self, fitness=[0], smoothing=0, smooth_style='non-bmu', saveAnim=False, plot_type='component', arrows=False, arrow_start=None, labels=False, inverse=True, default_fitness=0):
        """Plot the 3D SOM, with the fitness as the z axis
        method= {'component','umatrix'}
        
        smooth_style:       string <'all','non-bmu'>, whether to average the
                            fitness value of all cells or only the non-bmu cells
        smoothing:          The radius that determines the neighbourhood which
                            is used to average the fitness value.
                            
        arrows:             Whether or not to draw arrows linking a network to
                            a fitter network that has the lowest Euclidean
                            distance, can only be called True if a fitness
                            argument is provided.
        arrow_start:        integer indicating the sample for which to draw its
                            trajectory to the fittest network. By default it is
                            set to None and will draw arrows for all networks if
                            arrows==True.
        """
        try:
            self.codebook
            self.bmus
        except AttributeError:
            print('No codebook or BMUs found, make sure the SOM is trained or properly loaded.')
            exit()
        
        if smooth_style not in ['all','non-bmu']:
            print("Invalid smooth style provided, choose from 'all','non-bmu'")
            print("'non-bmu' is chosen as default")
            smooth_style = "non-bmu"
        
        
        self.arrows = arrows
        self.arrow_start = arrow_start
        self.smooth_style = smooth_style
        self.plane_no = 0
        self.clust_no = 0
        self.fitness = fitness
        self.labels = labels
        self.nan_replace = default_fitness # Value with which to replace NaN's in the fitness matrix
        print("Creating Fitness matrix...")
        self.fit_matrix = self.create_fitness_matrix(fitness, inverse=inverse, smoothing=smoothing)
        
        if arrows:
            print("Identifying arrow sources and targets...")
            self.target_list = self.find_neighbours()
        
        print("Plotting ...")
        #===============Plot the main graph==============
        self.fig = plt.figure()
        self.plot_type = plot_type
        self.prep_graph(labels=self.labels, arrows=self.arrows)
        self.draw_widgets()

        if saveAnim:
            animate_graph(fig, self.ax, filename='test111.gif')
        
        self.fig.canvas._master.geometry('1000x1000+0+0')
        plt.show()
        self.fig.clear()
        
    def draw_arrows(self, target_list, scalar=1):
        """
        Connects the networks with 3D arrows.
        
        target_list:    list containing a tuple for each unique BMU in the
                        format (target, weighted_distance) where the source for
                        the arrow is the index of the tuple.
                        
                        Alternatively, if arrow_start is set, the format is
                        (source, target, weighted_distance), so not the index
                        that determines the source.
        """
        bmus = self.bmus
        bmu_uni = self.bmu_uni
        z = self.fit_matrix
        for i in range(len(target_list)):
            if target_list[i] != None:
               
                if self.arrow_start != None:
                    xx = target_list[i][0][0]
                    yy = target_list[i][0][1]
                    t = bmus[target_list[i][1]] #target/neighbour (x,y) coords
                    minweight = target_list[i][2] #weighted difference between from and to
                else:
                    xx = bmus[bmu_uni[i]][0]# + 0.5
                    yy = bmus[bmu_uni[i]][1] #+ 0.5
                    t = bmus[target_list[i][0]] #target/neighbour (x,y) coords
                    minweight = target_list[i][1] #weighted difference between from and to
                    
                zz = z[yy,xx] #+ 0.5*scalar
                
                minweight = 0.999**minweight #Scale to obtain the right colour resolution
                col = [1- minweight, minweight,0] #colour of the arrows
                
                #set target/neighbour (x,y,z) values
                xx_t = t[0]# + 0.5
                yy_t = t[1]# + 0.5
                zz_t = z[yy_t,xx_t]#+ 0.5*scalar
                
                
                #draw the arrow
                p = Arrow3D([xx,xx_t],[yy,yy_t],[zz,zz_t], mutation_scale=20, lw=1, arrowstyle="fancy", color=col)#"g")
                self.ax.add_artist(p)
        
    def find_neighbours(self):
        """For each sample in the data, find a 'target' sample to which it is connected,
        i.e. which is the next step in evolution given a set of rules.
        
        Perhaps this should be changed to not find a nearest neighbour for each sample but rather, when starting with a low fitness sample
        find the nearest neighbour (even if it is a non-sample) which increases the fitness."""
        bmus = self.bmus
        target_list = []
        fit_matrix = self.fit_matrix
        bmu_uni = self.bmu_uni
        
        mw = 1 #mutation weight #3
        fw = 0  #fitness weight #15
        import warnings
        
        # Numpy warnings when requesting nanmin on an array only containing NaN's
        warnings.simplefilter("ignore") 
        if self.arrow_start == None: #Draw arrows for all BMUs
            for i in self.bmu_uni:
                q = bmus[i] #From point
                xx = q[0]
                yy = q[1]
                varibs = self.codebook[yy,xx]

                mismatches = [np.linalg.norm(varibs - self.codebook[bmus[x][1],bmus[x][0]]) 
                                if fit_matrix[bmus[x][1],bmus[x][0]] < fit_matrix[bmus[i][1],bmus[i][0]] 
                                else np.nan for x in bmu_uni]                
                
                delta_fit = [fit_matrix[bmus[x][1],bmus[x][0]] - fit_matrix[bmus[i][1],bmus[i][0]] for x in bmu_uni]

                weight_diff = [mismatches[x] * mw + delta_fit[x] * fw for x in range(len(mismatches))] #the lower the better
                minweight = np.nanmin(weight_diff)
                if np.isnan(minweight):
                    target_list.append(None)
                    continue
                w_index = weight_diff.index(minweight) #index of the selected neighbour in the non-duplicated network list
                
                target_list.append((bmu_uni[w_index], minweight))
        else: #Only draw arrows starting at the specified network
            if type(self.arrow_start) == int and self.arrow_start <= len(self.bmus):
                i = self.arrow_start
                while True:
                    q = bmus[i] #From point
                    xx = q[0]
                    yy = q[1]
                    varibs = self.codebook[yy,xx]
                    
                    mismatches = [np.linalg.norm(varibs - self.codebook[bmus[x][1],bmus[x][0]]) 
                                if fit_matrix[bmus[x][1],bmus[x][0]] < fit_matrix[bmus[i][1],bmus[i][0]] 
                                else np.nan for x in bmu_uni]                
                
                    delta_fit = [fit_matrix[bmus[x][1],bmus[x][0]] - fit_matrix[bmus[i][1],bmus[i][0]] for x in bmu_uni]
                    weight_diff = [mismatches[x] * mw + delta_fit[x] * fw for x in range(len(mismatches))] #the lower the better
                    
                    
                    minweight = np.nanmin(weight_diff)
                    if np.isnan(minweight):
                        break
                    w_index = weight_diff.index(minweight) #index of the selected neighbour in the non-duplicated network list
                    target_list.append((q, bmu_uni[w_index], minweight))
                    i = bmu_uni[w_index]
                    #print(delta_fit[w_index])
               
            else:
                print("WARNING: No valid arrow_style is provided, it should be \
                        an integer corresponding to the network from which to start the arrow trajectory")
        warnings.simplefilter("default")
        return(target_list)

    
    def find_same_cells(self,cell, thresh=0):
        """Finds the cells within a certain Euclidean distance of a reference cell"""
        cells = self.codebook
        return_list = []
        for i in range(self.codebook.shape[0]):
            for j in range(self.codebook.shape[1]):
                target = self.codebook[i,j]
                dist = np.linalg.norm(cell - target)
                if dist <= thresh:
                    return_list.append((i,j))
        return(return_list)
    
    def create_fitness_matrix(self,fitness, inverse=False, gaps=True, smoothing=3):
        import warnings
        
        """Give cells with the same component planes the same fitness value
        Try to find a similarity method so cells that aren't exactly the same can still be assigned a value,
        Perhaps by averaging their nearest matches in BMUs
        
        fitness:    list where each entry corresponds to the fitness of the
                    sample of the same index
                    
        inverse:    Boolean, whether to make the fitness values negative, which boils
                    down to flipping the map upside down
        gaps:       Boolean, whether to show gaps instead of cells which are  
                    not BMUs, since no fitness can be inferred
        
        smoothing:  integer, cells that are not a BMU can infer their fitness
                    from neighbouring BMU cells. This value indicates the radius
                    that defines the neighbourhood.
        """
        self.scalar = [1,-1][inverse]
        #fit = np.zeros((self.codebook.shape[0]+1,self.codebook.shape[1]+1)) #+1,+1
        if fitness.count(0) == len(fitness): #If an actual fitness matrix is provided
            return(np.zeros((self.codebook.shape[0]+1,self.codebook.shape[1]+1)))
        if gaps:
            fit = np.empty((self.codebook.shape[0]+1,self.codebook.shape[1]+1)) #+1,+1
            fit[:] = [min(fitness),max(fitness)][inverse] - 2*self.scalar #22 #set default fitness value
        

        # For smoothing out sometimes the mean of an empty array is requested 
        # and it defaults to NaN, which is perfectly fine so we ignore the warning
        warnings.simplefilter("ignore") 
        fit = [[np.nanmean([fitness[x] for x in np.where((self.bmus == 
            (row, col)).all(axis=1))[0]]) for row in range(self.codebook.shape[0])] 
            for col in range(self.codebook.shape[1])]
        
        fit = np.array(fit)
        
        
        # Smoothen landscape
        # Yes, I am aware that there are 5 for statements in that list comprehension
        sc = np.floor(smoothing/2)
        if self.smooth_style == 'non-bmu':
            # This only smoothens the non-bmu cells
            fit = [[np.nanmean([x for x in [fit.take([row-sc+i], mode = 'wrap', axis=0).take([col-sc+j], mode='wrap', axis=1) 
                            for i in range(smoothing) for j in range(smoothing)] if not np.isnan(x)] )
                            if [col, row] not in self.bmus.tolist() else fit[row,col] for col in range(fit.shape[1])] #N.B. [X,Y] for bmus so [col, row]
                            for row in range(fit.shape[0])]
        
        else:
            # This smoothens all cells
            # nanmean takes the mean while ignoring NaN's
            fit = [[np.nanmean([x for x in [fit.take([row-sc+i], mode = 'wrap', axis=0).take([col-sc+j], mode='wrap', axis=1)  
                            for i in range(smoothing) for j in range(smoothing)] ] ) 
                            for col in range(fit.shape[1])]
                            for row in range(fit.shape[0])]
        
        # Make sure other warnings are displayed again
        warnings.simplefilter("default")
        
        fit = np.array(fit)
        fit = np.where(np.isnan(fit), self.nan_replace, fit)
        #fit = np.nan_to_num(fit)

        return(fit)
    
    #==============================QUALITY CHECKS===============================================
    def stress_function(self,d,D):
        """See background paper
        Defined as: Sum((d(i,j) - D(i,J))**2)/Sum(D(i,j)**2) over all datapoints i,j
        d(i,j) : distance between original data i and j (probably needs normalizing)
        D(i,j) : distance between mapped data i and j
        """
        d = d/d.max() #Normalize data distance
        top = (d-D)**2
        top = sum([sum(x) for x in top])
        bottom = sum([sum(x) for x in (D**2)])
        S = top/bottom
        return(S)
    
    def sammon_stress(d,D):
        """Sammon stress is a stress function for non-linear mappings"""
        d = d/d.max() #Normalize data distance, not sure if we should use this
        rh = sum([sum(x) for x in ((0.5*(d-D))**2)/d]) #right hand of the equation
        #rh = rh  #since the distance matrix is symmetric all distances are counted double
        lf = 1/(0.5*sum([sum(x) for x in d]))

        return(lf*rh)
    
    def separation_score(self):
        """How well does the plot represent the Euclidean distance between datapoints?
        To answer this we construct 2 2D-matrices, A and B.
        A is the Euclidean distance matrix
        B is the datapoint distance matrix
        We find c, which is the correlation value between the two matrices,
        the higher the correlation the better the plot represents the Euclidean distance"""
        
        #Calculate Euclidean distance matrix A
        A = np.array([])
        for sample in self.data:
            dist = [np.linalg.norm(sample-s) for s in self.data]
            if A.shape[0] == 0:
                A = np.array(dist)
            else:
                A = np.vstack([A, dist])
        #Calculate relative datapoint distance matrix B
        B = np.array([])
        for sample in self.bmus:
            x_min, x_max = np.min(self.bmus, 0), np.max(self.bmus, 0)
            denom = np.linalg.norm(x_max - x_min) # for normalizing
            if self.toroid == True:
                dist = np.array([np.linalg.norm(np.minimum(abs(sample-s), self.codebook.shape[:2] - abs(sample-s)))/denom for s in self.bmus])
            else:
                dist = np.array([np.linalg.norm(sample-s)/denom for s in self.bmus])
            if B.shape[0] ==0:
                B = dist
            else:
                B = np.vstack([B,dist])

        Af = A.flatten()
        Bf = B.flatten()
        c = np.corrcoef(Af,Bf)
        
        if np.isnan(c[0][1]):
            print('Correlation is NAN' )

        return(c[0][1], self.stress_function(A,B))
        
    def quality_check(self):
        qc = self.separation_score()
        print('Correlation between LD and HD Euclidean distance:',qc[0])
        print('Stress between LD and HD Euclidean distance:\t ',qc[1])
    
    def topographic_error(self, data, rad=2, report_index=True):
        """Calculates the percentage of samples whose BMU is within a certain radius of the second BMU"""
        var = som.codebook
        err_list = []
        for q in range(len(som.bmus)):
            j = self.bmus[q][0] #N.B. COLUMN
            i = self.bmus[q][1] #N.B. ROW
            vec = data[q]
            NN = [[np.linalg.norm(var[x,y] - vec) if (i,j) != (x,y) else 9999999999 for y in range(var.shape[1])] for x in range(var.shape[0])]
            mn = [min(x) for x in NN]
            lowest = min(mn)
            row = mn.index(lowest)
            col = NN[row].index(lowest)
            if np.sqrt((i-row)**2 + (j-col)**2) > rad:
                err_list.append(1)
            else:
                err_list.append(0)
    
        if report_index:
            lis = [str(x) for x in range(len(err_list)) if err_list[x] == 1]
        return(sum(err_list)/len(err_list))
    
    #==============================CLUSTERING/TREES============================================
    def create_extended_umatrix(self):
        """
        Rectangular:
        Normal U-matrix only contains summed distances to its neighbour,
        this matrix will give an explicit representation of the distance to all its neighbours
        Format: Rx2C array where 
        [0,0] refers to the distance of point 0,0 to 0,1
        [0,1] refers to the distance of point 0,0 to 1,0
        i.e. a chessboard pattern of [neighbour right, neighbour below, repeat...]
        Hexagonal:
        Similar to rectangular extended U-matrix, except the pattern is:
        [Right, RightBelow, LeftBelow]
        N.B. Left-Up, does not always refer [R-1,C-1], because of the offsets in a hexagonal map, rather it is:
        [R-1,C-1*(R%2)] and [R-1,C+1-1*(R%2)]
        """
        if type(self.codebook) == type(np.array([])): #Check if the SOM is trained or loaded
            #======Rectangular grid======
            if self.gridtype == 'rectangular':
                ext_uma = np.array([
                        [np.linalg.norm(self.codebook[rN, int(cN/2)] - self.codebook[rN,(int(cN/2)+1)%self.codebook.shape[1]]) 
                            if (cN%2 == 0) # and int(cN/2) < (self.codebook.shape[1] -1))  #if it is the first of a pair and not the last column --> compare it to its right neighbour
                        else np.linalg.norm(self.codebook[rN, int(cN/2)] - self.codebook[(rN+1)%self.codebook.shape[0],int(cN/2)]) 
                            if (cN%2 == 1) # and rN < (self.codebook.shape[0]-1))  # if it is the second of a pair and not in the last row --> compare it to its lower neighbour
                        else 0  for cN in range(self.codebook.shape[1]*2) #otherwise give it a 0
                        ] for rN in range(self.codebook.shape[0])
                    ])
                    
            #======Hexagonal grid======
            elif self.gridtype == 'hexagonal':
                ext_uma = np.array([[np.linalg.norm(self.codebook[rN, int(cN/3)] - self.codebook[rN, (int(cN/3)+1)%self.codebook.shape[1]])  #RIGHT
                if  (cN%3 == 0) #and (int(cN/3) < (self.codebook.shape[1] -1 ))) #If not at the right edge and the first entry
                    else np.linalg.norm(self.codebook[rN, int(cN/3)] - self.codebook[(rN+1)%self.codebook.shape[0], (int(cN/3)+([1,0][rN%2]))%self.codebook.shape[1]])  #RIGHT BELOW
                if  (cN%3 == 1) # and ((int(cN/3)+([1,0][rN%2])) < (self.codebook.shape[1]))) and (int(rN) < (self.codebook.shape[0] -1))) #If we are not at the right or bottom edge
                    else np.linalg.norm(self.codebook[rN, int(cN/3)] - self.codebook[(rN+1)%self.codebook.shape[0], (int(cN/3)+[1,0][rN%2] -1)%self.codebook.shape[1]])
                if  (cN%3 == 2) # and ((int(cN/3)+[1,0][rN%2] - 1) >= 0) and (int(rN) < (self.codebook.shape[0] -1))) #LEFT BELOW
                    else 0 for cN in range(self.codebook.shape[1]*3)
                ]for rN in range(self.codebook.shape[0])])
        else:
            
            print('Train the SOM before requesting the extended U-matrix!')
        return(ext_uma)
        
    
    def create_uma(self):        
        #print(self.codebook.take([-2, -1, 0], mode="wrap", axis
        """ Creates the Unified Distance Matrix, aka the U-Matrix
        """
        uma = [[np.linalg.norm(self.codebook.take([x for x in range(row-1,row+2)], mode="wrap", 
            axis=0).take([x for x in range(col-1,col+2)], mode="wrap", axis=1) - self.codebook[row,col]) 
            for col in range(self.codebook.shape[1])] for row in range(self.codebook.shape[0])]

        return(uma)
        
    def get_tree(self, n_thresholds = 200, dont_create_newick=False):
        """Construct a TreeSOM and cluster the trees to get a consensus tree
        
        n_thresholds:   Integer value indicating how many threshold values 
                        should be checked: Starting threshold of 0 is 
                        incremented each iteration with 1/n_thresholds until it 
                        reaches 1.
                        
        returns:    a string representing the tree in Newick format"""
        if type(self.bmus) == None:
            print('Please train the SOM before requesting the tree.')
            return()
        
        self.n_thresholds = n_thresholds
        tree_clust = {}
        thresh_range = [float(x/n_thresholds) for x in range(0,n_thresholds+1,1)] #as percentage of the maximum distance between two adjacent cells in the codebook
        ext_uma = self.create_extended_umatrix() #Matrix depicting the distance from each cell to its neighbours
        self.clustrs = {}
        self.bmu_clust = {}
        for thresh in thresh_range:
            self.clustrs[thresh] = self.identify_clusters(ext_uma,thresh)
            self.bmu_clust[thresh] = dict([('Net' + str(x),self.clustrs[thresh][self.bmus[x][1],self.bmus[x][0]]) for x in range(len(self.bmus))])
            if len(set([x for x in self.clustrs[thresh].values()])) == 1:
                # All nodes are in the same cluster
                print("Clusters are all the same after threshold %.2f" %(thresh))
                break
        if dont_create_newick:
            return
        self.tree = self.make_newick(self.bmu_clust)
        return(self.tree)
        #self.tree = Phylo.read(StringIO(self.tree),'newick')
        
    
    def make_newick(self,clust_data=None,savefile=None):
        """Converts the dictionary with cluster labels to the Newick format
        
        clust_data:     dictionary, with threshold values as keys, where each
                        entry contains another dictionary in the format:
                        'Net#':clust_id
        
        returns:        Newick format string representing the tree belonging to
                        the clusters
                        
        """
        sorted_keys = [x for x in clust_data.keys()]
        sorted_keys.sort()
        
        current_cluster = tuple([x for x in clust_data[0].keys()])
        
        keep_list = []
        layerlist = []
        unique_list = []
        tot_clust = []
        cluster_dict = {}
        
        # At which thresholds do the clusters change?
        for key in sorted_keys:
            layerlist.append([])
            skipp = []
            for x in clust_data[key]:
                if x not in skipp:
                    same = [i for i in clust_data[key] if clust_data[key][i] == clust_data[key][x]]
                    for s in same:
                        skipp.append(s)
                    layerlist[sorted_keys.index(key)].append(same)#
            
            if sorted_keys.index(key) > 0 and layerlist[-2] == layerlist[-1]:
                # Skipping
                pass
            else:
                keep_list.append(key)
                unique_list.append(layerlist[-1])
        
        
        newick_dict = {}
        for l in range(len(keep_list)):
            level = keep_list[l]
            cluster_dict[level] = []
            newick_dict[level] = []
            skip_list = []
            for cluster in set(clust_data[level].values()):
                if cluster not in skip_list:
                    if l==0:
                        to_add = [x for x in clust_data[level] if clust_data[level][x] == cluster]
                        to_add_str = str([str(x) + ':0.00000' for x in to_add]) + ':%.5f' %(level) 
                    else:
                        to_add = [lowest_list(x) for x in cluster_dict[keep_list[l-1]] if len(lowest_level(x)) > 0 and clust_data[level][lowest_level(x)] == cluster]
                        to_add_str = [newick_dict[keep_list[l-1]][x] for x in range(len(cluster_dict[keep_list[l-1]])) if len(lowest_level(cluster_dict[keep_list[l-1]][x])) > 0 and clust_data[level][lowest_level(cluster_dict[keep_list[l-1]][x])] == cluster]
                        to_add_str = '%s:%.5f' %(str(to_add_str).replace('"',''),level-keep_list[l-1]) #adds threshold distance as edge length
                    cluster_dict[level].append(to_add) 
                    newick_dict[level].append(to_add_str)

        newick = ','.join(newick_dict[keep_list[-1]])
        newick = newick.replace('"','').replace("'",'')
        newick = newick.replace('[','(').replace(']',')') + ";" #Go from list to tuple
        newick = newick.replace(', ',',')
        return(newick)


    def cluster(self,node, ext_uma):
        """
        Can the target be reached from the starting node without crossing a 
        cell which has a larger distance than the set threshold to its neighbour
        
        node:       Tuple of the format (row no, column no)
        ext_uma:    Extended U-matrix, 2D matrix following the format described
                    in create_extended_umatrix() 
        """
        self.clust[node] = self.C
        self.unvisited_nodes.remove(node)
        for n in self.reachable_neighbours(node,ext_uma):
            if n in self.unvisited_nodes:
                self.cluster(n, ext_uma)
        
    
    def reachable_neighbours(self, node, ext_uma):
        """
        Checks which of the four neighbouring cells are deemed reachable given
        threshold self.T
        
        node:       Tuple of the format (row no, column no)
        ext_uma:    Extended U-matrix, 2D matrix following the format described
                    in create_extended_umatrix()
        """
        
    
        # RECTANGULAR CELLS (4 neighbours)
        
        reachable = []
        if self.gridtype == 'rectangular':
            #======Below======
            if node[0] < (ext_uma.shape[0] - 1):
                if ext_uma[node[0],node[1]*2+1] < self.T:
                    reachable.append((node[0]+1,node[1]))
            elif node[0] == (ext_uma.shape[0] -1) and self.toroid == True: #node is at the edge, important for toroid map
                if ext_uma[node[0],node[1]*2+1] < self.T:
                    reachable.append((0,node[1])) # first node on the other side of the map
            
            #======Above======
            if node[0] > 0: 
                if ext_uma[node[0]-1,node[1]*2+1] < self.T:
                    reachable.append((node[0]-1,node[1]))
            elif node[0] == 0 and self.toroid == True: # wrap around to the bottom of the map
                if ext_uma[ext_uma.shape[0]-1,node[1]*2+1] < self.T:
                    reachable.append((ext_uma.shape[0]-1,node[1]))
            
            #======Right======
            if node[1] < (self.codebook.shape[1]-1):
                if ext_uma[node[0],node[1]*2] < self.T:
                    reachable.append((node[0],node[1]+1))
            elif node[1] == (self.codebook.shape[1]-1) and self.toroid == True: # wrap around to the left of the map
                if ext_uma[node[0],node[1]*2] < self.T:
                    reachable.append((node[0],0))
            
            #======Left======
            if node[1] > 0:
                if ext_uma[node[0],(node[1]-1)*2] < self.T:
                    reachable.append((node[0],node[1]-1))
            elif node[1] == 0 and self.toroid == True: # wrap around to the right of the map
                if ext_uma[node[0], ext_uma.shape[1]-2] < self.T:
                    reachable.append((node[0],self.codebook.shape[1]-1))
            return(reachable)
        
        
        # HEXAGONAL CELLS (6 Neighbours)
        
        elif self.gridtype == 'hexagonal':
            #if toroid == True:                                                  
            if ext_uma[node[0],node[1]*3] < self.T: # RIGHT
                reachable.append((node[0],(node[1]+1)%self.codebook.shape[1]))
            if ext_uma[node[0],node[1]*3+1] < self.T: # RIGHT BELOW
                reachable.append(((node[0]+1)%self.codebook.shape[0], (node[1] + [1,0][node[0]%2])%self.codebook.shape[1]))                        
            if ext_uma[node[0], node[1]*3+2] < self.T: # LEFT BELOW
                reachable.append(((node[0]+1)%self.codebook.shape[0], (node[1] - 1*(node[0]%2))%self.codebook.shape[1])) 
                                                                                            
            if ext_uma[node[0],((node[1]-1)*3)%ext_uma.shape[1]] < self.T: # LEFT
                reachable.append((node[0],(node[1]-1)%self.codebook.shape[1]))                                                 
            if ext_uma[(node[0]-1)%ext_uma.shape[0], ((node[1] - (node[0]%2))*3+1)%ext_uma.shape[1]] < self.T: # LEFT UP
                reachable.append(((node[0]-1)%self.codebook.shape[0], (node[1]-(node[0]%2))%self.codebook.shape[1]))                          
            if ext_uma[(node[0]-1)%ext_uma.shape[0], ((node[1] + [1,0][node[0]%2])*3+2)%ext_uma.shape[1]] < self.T: # RIGHT UP
                reachable.append(((node[0]-1)%self.codebook.shape[0], (node[1] + [1,0][node[0]%2])%self.codebook.shape[1]))

            return(reachable)
            
        
        
    
    def identify_clusters(self, ext_uma, T):
        """Identify clusters in the extended U-matrix, given a tolerated distance T
        ext_uma:        numpy array for the extended U-matrix, see function 
                        create_extended_uma for specifics
        
        T:              float value indicating clustering threshold
        
        Returns:        Dictionary of the format {(row,col): cluster}
                        where each value corresponds to the cluster the cell at
                        [row,col] is in"""
        self.T = ext_uma.max()*T #Threshold as percentage of the observed maximum distance between two nodes
        rows = self.codebook.shape[0]#ext_uma.shape[0]
        cols = self.codebook.shape[1]
        
        self.clust = {}
        self.unvisited_nodes = [(r,c) for c in range(cols) for r in range(rows)]

        self.C = 0 #current cluster
        while len(self.unvisited_nodes) > 0:
            node = self.unvisited_nodes[0]
            self.C+=1
            self.cluster(node, ext_uma)
        return(self.clust)

def lowest_list(x):
        while True:
            if type(x) == type('') or len(x) > 1:
                break
            else:
                x=x[0]
        return(x)

def lowest_level(x):
    """Used in the construction of the Newick string, finds the lowest level
    in a list, e.g. lowest_level([[['a']]]) would return 'a'
    """
    while True:
        if type(x) == type(''):
            return(x)
        else:
            try:
                x=x[0]
            except IndexError: # Lowest level reached on empty cluster
                return(x)
    return(x)
    
def create_missing_dirs(save_folder):
    dirs = [x for x in str(save_folder).split('/')][:-1]
    for i in range(len(dirs)):
        ndir = '/'.join(dirs[0:i+1])
        if not os.path.isdir(ndir):
            os.mkdir(ndir)

def data_from_csv(filename,fitness_filename, deduplicate=False):
    """
    Load parameter data from .csv file, to remove duplicate entries set deduplicate=True
    """
    
    """
    data = np.genfromtxt(filename, delimiter=',', skip_header=1)
    lis = [x for x in range(1,data.shape[1])]
    data = data[:,lis]
    """
    
    with open("merged_variables.csv") as f:
        data = np.array([[float(y) for y in x.split(',')[1:-1]] for x in f.readlines()[1:]])
    if fitness_filename:
        fitness = [float(x.strip()) for x in open(fitness_filename).readlines()]
    else:
        #print('\tNo fitness file provided, only flat landscapes can be plotted now')
        fitness = [0 for x in range(data.shape[1])]
    
    if deduplicate:
        b = np.ascontiguousarray(data).view(np.dtype((np.void, data.dtype.itemsize * data.shape[1])))
        _, idx = np.unique(b, return_index=True)
        idx.sort()
        data = data[idx]
        fitness = [fitness[x] for x in range(len(fitness)) if x in idx]

    return(np.float64(np.array([x[1:] for x in data])),fitness)

def create_consensus_tree(tree_list, save_folder, save_identifier = 0, consensus_threshold=0.5):
    """Creates consensus tree by calling an R script
    tree_list:      list of ####
    save_folder:    string indicating the folder in which to save the image
    save_indentifier:   string or number used to identify the save file
    
    """
    if not save_folder.endswith('/'):
        save_folder = save_folder + '/'
    fname_all = save_folder + '%s_all_trees.tree' %(str(save_identifier))
    fname_cons = save_folder + '%s_majority_consensus.pdf' %(str(save_identifier))
    
    
    create_missing_dirs(save_folder)
    with open(fname_all,'w') as outp:
        outp.write('\n'.join(tree_list))
    
    cmd = ['Rscript','Phyl.R', fname_all, fname_cons, '%.2f'%(consensus_threshold)]
    print('\nRunning R script to construct consensus tree...')
    try:
        subprocess.check_call(cmd, universal_newlines=True)
        print('Construction of consensus tree complete')
        print('The PNG is saved as %s'%(fname_cons))
    except subprocess.CalledProcessError as err:
        print(err)
        print('Construction of consensus tree failed in the R script!\n')
        print('All the trees are saved in %s, which can be manually loaded in the R script to construct the consensus tree.'%(save_folder))

def region_parameters(my_som, bmus, variable_list, threshold=5.0, only_bmus=False, toroid=False):
    """
    Shows the parameters that are active in a region spanned between a set of given BMUs
    only the parameters which have an activity above the provided threshold will be shown.
    If the only_bmus argument is set to True it will only return the BMU parameters above the threshold.
    
    
    
    In:         my_som,         SOM object
                bmus,           numpy array of Best-matching units
                variable_list,  list with variable identifiers that span the region of interest
                threshold,      threshold float value, indicating the minimal activity of a variable
                                below which the variable is not considered important
                only_bmus,      Boolean value indicating whether or not the variables of
                                non-BMUs are considered
    Returns:    a gene family dictionary with their relative presence as values,
                i.e. the percentage of networks a certain gene family is involved in
    """
    
    optimum_list = []
    for b in bmus:
        optimum_list.append(my_som.bmus[b])

    col_range = [np.min([x[0] for x in optimum_list]),np.max([x[0] for x in optimum_list])]
    row_range = [np.min([x[1] for x in optimum_list]),np.max([x[1] for x in optimum_list])]

    indices = my_som.codebook[row_range[0]:row_range[1],col_range[0]:col_range[1]] > threshold
    param_list = []

    if only_bmus: # Only check BMU cells for parameters
        no_cells = len(bmus)
        for b_id in bmus:
            b = my_som.bmus[b_id]
            indices = my_som.codebook[b[1],b[0]] > threshold
            param_list.append([variable_list[x] for x in range(len(variable_list)) if indices[x] == True])
    else: # Check all the cells
        no_cells = (row_range[1]-row_range[0])*(col_range[1]-col_range[0])
        for i in range(row_range[1] - row_range[0]):
            for j in range(col_range[1] - col_range[0]):
                param_list.append([variable_list[x] for x in range(len(indices[i,j])) if indices[i,j,x] == True])
        
    

    node_sets = [set([y[:y.index(']')+1] for y in x]) for x in param_list] #Set of nodes for each network
    tot_set = set()
    for n in node_sets:
        tot_set = n | tot_set

    node_count = dict([(y,sum([1 if y in x else 0 for x in node_sets])/no_cells) for y in tot_set])

    return(node_count)


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)

