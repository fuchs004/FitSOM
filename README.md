Tensorflow implementation of a Self-Organizing Map algorithm. 
Specifically created to be suited for multiple runs, i.e. initialization of the graph component is independent of the training operations.
This code allows the user to visualize the SOM in 3D, the third dimension is not used in training but can be used for labeling the samples,
e.g. to create an evolutionary fitness landscape (see Lorenzo-Redondo et al. 2014)
To check the quality of a trained som the check_quality() function can be used, which returns two values:
    The correlation between the LD-distance matrix and HD-distance matrix
    The 'stress' between the LD and HD mapping

Additionally, this code also features a SOM-clustering algorithm as well as a functions which forms a phylogenetic tree from said clustering. These trees can then be used
to form a consensus tree. All SOMs used to create these trees can easily be saved and thus loaded in later on to visualize the SOM that best represents a given consensus tree.
The consensus tree algorithm is provided separately in an .R file, but the generation of the consensus trees is automated via a subprocess call.
