
from FitSOM import *

import os
import numpy as np
import matplotlib.pyplot as plt

def example_iris():
    """Iris dataset example"""
    raw_dat = open("Data/Iris.csv").read().split('\n')
    dat = np.array([[float(y) for y in x.split(',')[1:-1]] for x in raw_dat[1:]][:-1])
    fit = np.array([x.split(',')[-1] for x in raw_dat[1:]][:-1])
    labels = sorted(list(set(fit)))
    fit = [labels.index(x) for x in fit]
    
    # Dimensions of the SOM
    dim = [int(np.sqrt(dat.shape[0])),int(np.sqrt(dat.shape[0]))]

    
    new_som = SOM(dat,dim, epochs=3500, initial_lr=0.5, gridtype= 'hexagonal', toroid=True)
    new_som.train()
    new_som.plot(fit, labels = labels, plot_type='umatrix')
    
    # Constructing a majority consensus tree for this dataset
    # The output is a phylogenetic tree in Newick format, additionally an
    # R script generates a .png file, but for large trees it's best to use the
    # Newick file to create your own image.
    tree_list = []
    for i in range(10):
        new_som.train()
        new_som.quality_check()
        tree_list.append(new_som.get_tree())
        print(i)

    create_consensus_tree(tree_list, 'IrisTree', save_identifier=0)
    del(new_som)
    
    
def example_EA_data(load_file=None):
    if load_file == None:
        with open("Data/merged_variables_r1.csv") as f:
            dat = np.array([[float(y) for y in x.split(',')[1:] if y.strip() != ''] for x in f.readlines()[1:]])
            # dat[dat == 0] = -15 # Optional penalty
        fit = [float(x) for x in open('Data/merged_fitness_r1.txt').readlines()]
        dim = [int(np.sqrt(dat.shape[0])-20), int(np.sqrt(dat.shape[0])-20)]
        
        my_som = SOM(dat, dim, epochs=3500, initial_lr=0.2, gridtype='hexagonal', toroid=True)
        print('Training SOM...')
        my_som.train()
        print('SOM trained.')
        my_som.save(filename='SavedSOM/r1_e3500_l02_medium')
        print('SOM Saved.')
    else:
        my_som = SOM(load_file=load_file)
        
    fit = [float(x) for x in open('Data/merged_fitness_r1.txt').readlines()]
    print('Plotting SOM...')
    # Plot the SOM, with the fitness as z-axis, some smoothing applied to cells
    # that are not BMUs (by inferring fitness from neighbouring cells).
    # Non-bmu cells have a default fitness of 20 (worst score for in the EA).
    my_som.plot(fitness=fit, smoothing=3, default_fitness=20, arrows=True, arrow_start=11)



def example_EA_tree():
    with open("Data/variables_42.csv",'r') as inp:
        dat = np.array([[float(y) for y in x.split(',')[1:-1]] for x in inp.read().split('\n')[1:]][:-1])

    fit = [float(x) for x in open('Data/fitness_42.txt').readlines()]


    labels = range(dat.shape[0])
    dim = [int(np.sqrt(dat.shape[0])),int(np.sqrt(dat.shape[0]))]
    new_som = SOM(dat, dim, epochs=3500, initial_lr=0.2, gridtype='hexagonal', toroid=True)

    tree_list = []
    for i in range(1):
        new_som.train()
        new_som.quality_check()
        new_som.plot(fit, smoothing=3, arrows=True, labels=True)
        tree_list.append(new_som.get_tree())
        print(i)

    create_consensus_tree(tree_list, '42_revised', save_identifier=0)
    
if __name__ == "__main__":
    
    # Example of the merged EA data
    #example_EA_data(load_file="SavedSOM/r1_e3500_l02_medium")
    
    # Example for a single EA run
    #example_EA_tree()
    
    # Example of the famous iris dataset
    #example_iris()
