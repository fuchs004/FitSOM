import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import itertools
import os

def contains_exact_motif(query, file_dir):
    if not file_dir.endswith('/'):
        file_dir += '/'
    lisdir = [x for x in os.listdir(file_dir) if x.endswith(".csv")]
    attr_list = []
    
    for filename in lisdir:
        with open('%s%s'%(file_dir, filename)) as inp:
            attributes = [x.split(',') for x in inp.read().split('\n')]
            
    

def ROC(data, parameter_list, fitness_list, parameter_name, thresh_range=[16,17,18,19,20], print_networks=False):
    """ 
    Calculates the relative occurrence of a motif compared to the average
    relative occurence of all other motifs for a given fitness threshold.
    
    data:           list of lists, where each entry corresponds to an EA 
                    network and each embedded list to the parameter values
    parameter_list:  
    parameter_name: 
    print_networks: Boolean indicating whether to print the network IDs of the
                    networks containing the specified parameter_name.
    """
    param_indices = [] # which parameters corresponds to the motif of interest
    data_indices = [] # which networks contain the motif of interest (M.O.I.)
    
    motif_list = list(set([p[:p.index(']')+1] for p in parameter_list]))
    motif_count = [0 for _ in range(len(motif_list))]
    # Check whether a parameter name corresponds to the name we're looking for
    for idx, p in enumerate(parameter_list):
        name = p[:p.index(']')+1]
        if parameter_name == name:
            param_indices.append(idx)
    
    templist = []
    
    combined_member_list = []
    combined_nonmember_list = []
    avg_list = []
    std_list = []
    dat_m = []
    for thresh in thresh_range:
        motif_fitness = []
        non_motif_fitness = []
        motif_count = [0 for _ in range(len(motif_list))]

        for data_idx, d in enumerate(data):
            if fitness_list[data_idx] < thresh:
                
                found = False
                had_list = []
                for idx, x in enumerate(d):
                    if idx in param_indices:
                        if d[idx] > 0 and found == False:
                            if print_networks:
                                print(data_idx)
                            motif_fitness.append(fitness_list[data_idx])
                            found = True
                    elif x > 0:
                        p =  parameter_list[idx][:parameter_list[idx].index(']')+1]
                        if p not in had_list:
                            had_list.append(p)
                            motif_count[motif_list.index(p)] +=1
                    
                if found == False:    
                    non_motif_fitness.append(fitness_list[data_idx])

        #combined_member_list.append(motif_fitness)
        #combined_nonmember_list.append(non_motif_fitness)

        temp = [x for x in motif_count]
        templist.append(temp)
        #               avg count of other motifs / total_count
        avg_list.append((sum(temp)/len(temp))/(len(temp)+len(motif_fitness)))
        std_list.append((np.std(temp))/(len(temp)+len(motif_fitness)))
        #               motif count     / total count
        dat_m.append(len(motif_fitness)/(len(temp)+len(motif_fitness)))

    fig, ax = plt.subplots()
    markers = [',','o','*','+','.']
    c = 0
    for x,y in zip(avg_list,dat_m):
        plt.plot(x, y, marker='o')#, marker=markers[c%len(markers)])
        c+=1
    #mx = range(int(max(max(avg_list),max(dat_m))))
    
    avg_list = np.array(avg_list)
    std_list = np.array(std_list)
    print(avg_list-std_list)
    print(avg_list+std_list)
    print(templist)
    plt.plot(avg_list,avg_list,'--')
    plt.fill_between(avg_list, avg_list-std_list, avg_list+std_list,
        alpha=0.2, edgecolor='#1B2ACC', facecolor='#089FFF',
        linewidth=4, linestyle='dashdot', antialiased=True)
    #plt.plot(dat_nm)
    plt.title("Relative presence of motif vs average of all other motifs\n%s"%(parameter_name))
    ax.legend(['fit <%.2f'%(x) for x in thresh_range])
    #ax.legend(['Members/Non-members','Reference curve'])
    #plt.xticks(mx)
    #plt.yticks(mx)
    #ax.set_xticklabels([1/x for x in thresh_range])
    plt.xlabel('Relative average presence of non-members')
    plt.ylabel('Relative presence of members')
    plt.show()
    
def improp_ROC(data, parameter_list, fitness_list, parameter_name, thresh_ranges=[20,19,18,17,16], print_networks=False):
    """ 
    
    data:           list of lists, where each entry corresponds to an EA 
                    network and each embedded list to the parameter values
    parameter_list:  
    parameter_name: 
    print_networks: Boolean indicating whether to print the network IDs of the
                    networks containing the specified parameter_name.
    """
    param_indices = [] # which parameters corresponds to the motif of interest
    data_indices = [] # which networks contain the motif of interest
    
    motif_fitness = []
    non_motif_fitness = []
    
    # Check whether a parameter name corresponds to the name we're looking for
    for idx, p in enumerate(parameter_list):
        name = p[:p.index(']')+1]
        if parameter_name == name:
            param_indices.append(idx)
    
    for data_idx, d in enumerate(data):
        found = False
        for idx in param_indices:
            if d[idx] > 0:
                if print_networks:
                    print(data_idx)
                motif_fitness.append(fitness_list[data_idx])
                found = True
                break
        if not found: # If the network does not contain the motif
            non_motif_fitness.append(fitness_list[data_idx])
                
    print(sum(motif_fitness)/len(motif_fitness))
    print(sum(non_motif_fitness)/len(non_motif_fitness))   
            


def total_motif_occurence(som, fitness, fit_range,  seed, data_filename, clust_thresh=1):
    """Plots the relative occurence of a gene family in various samples, selected
    on basis of their fitness value and whether or not they exist in the same
    cluster given a sample of interest and a clustering threshold value.
    
    som:            trained SOM object
    fitness:        List containing the fitness of all the networks in the SOM,
                    in order.
    fit_range:      list containing the fitness/cost values, only samples with
                    fitness values below this threshold are considered in the
                    calculations.
    seed:           integer value indicating the sample being tracked, i.e.
                    the clusters we look at always contains this sample.
    data_filename:  location of the original .csv file containing the data
                    (samples on rows, parameters on columns)
    clust_thresh:   float value indicating the clustering threshold, it is the
                    fraction of allowed distance between two adjacent cells,
                    relative to the maximum distance between two adjacent cells,
                    for them to be considered clustered.
    
    """
    som.get_tree() # Make sure the clusters exist for each threshold value
    var_names = []
    tot_vars = []
    tot_n = [] # Sample count for each threshold in fit_range

    for fit_thresh in fit_range:
        cluster = som.bmu_clust[clust_thresh]
        sample_list = [int(x[3:]) for x in cluster if cluster[x] == cluster['Net%d'%seed] and fitness[int(x[3:])] < fit_thresh]
        common, n  = calculate_relative_motif_count(sample_list, data_filename = data_filename)
        keys = sorted(list(common.keys()))
        if len(var_names) == 0:
            var_names = keys
        tot_vars.append([common[var] if var in common else 0 for var in var_names])
        tot_n.append(n)
        #print([x for x in common if common[x] > 0.3])
        """
        fig, ax = plt.subplots()
        ax.bar(range(len(keys)), [common[x] for x in keys])
        ax.set_xticks(range(len(keys)))
        ax.set_title('Motif occurence in cluster at threshold %.2f in %d unique samples \nwith a evolutionary cost of <%.2f' %(clust_thresh, n, fit_thresh))
        ax.set_ylabel('Frequency')
        ax.set_xticklabels(keys, rotation='vertical')
        """
    with open('motif_detection.csv','w') as outp:
        outp.write(' ,' + ','.join(var_names))
        outp.write('\n')
        for i,x in enumerate(tot_vars):
            outp.write('%.2f,'%fit_range[i] + ','.join([str(y) for y in x]))
            outp.write('\n')

    fig, ax = plt.subplots()
    bars = [ax.bar(range(len(var_names)), tot_vars[0])]
    prev = tot_vars[0]
    
    for idx, i in enumerate(tot_vars[1:]):
        bars.append(ax.bar(range(len(var_names)), i, bottom=prev))
        prev = [prev[x] + i[x] for x in range(len(prev))]
        #prev = [p+tot_vars[idx][y] for y, p in enumerate(prev)]
    
    ax.set_xticks(range(len(var_names)))
    ax.set_title('Motif occurence in cluster at threshold %.2f' %(clust_thresh))
    ax.set_ylabel('Relative motif occurence')
    ax.set_xticklabels(var_names, rotation='vertical')
    ax.legend(bars, ['%.2f, n=%d' %(fit,tot_n[x]) for x, fit in enumerate(fit_range)])

    plt.show()

def calculate_relative_motif_count(samples, data_filename, remove_duplicates=True):
    """Calculates the relative occurence of motifs in all samples
    
    samples:        list with sample ids to be analyzed
    data_filename:  location of the .csv containing the data, with samples on
                    the rows and parameter values on the columns. The first row
                    and column are the names of the parameters and samples
                    respectively.
    remove_duplicates:  Boolean value, indicating whether or not to count
                        networks with the exact same set of non-zero parameters
                        once.
    
    returns:    tuple, where the first entry is a dictionary with motif counts
                and the second entry is the amount of samples that were
                considered in the calculation.
    """
    with open(data_filename,'r') as rf:
        sp = [x.split(',')[1:] for x in rf.read().split('\n')]
        var_names = sp[0]
        data = sp[1:]
        data = [d for idx, d in enumerate(data) if idx in samples]
    
    node_sets = [frozenset([var_names[y][:var_names[y].index(']')+1] for y in range(len(x)) if x[y] != '' and float(x[y]) > 0.0]) for x in data] #Set of nodes for each network
    if remove_duplicates:
        node_sets = set(node_sets)
    tot_set = set()
    
    for n in node_sets:
        tot_set = n | tot_set
    node_count = dict([(y,sum([1 if y in x else 0 for x in node_sets])/len(node_sets)) for y in tot_set])
    return(node_count, len(node_sets))

def plot_threshold_effect(samples,save_file=None, detection_threshold=0.01):
    reg_param = region_parameters(my_som, samples, variable_list, threshold=detection_threshold, only_bmus=True)
    y = []
    x = [x/100 for x in range(101)]
    for thresh in x:
        print(thresh)
        y.append(len([x for x in reg_param if reg_param[x] >= thresh]))
    
    fig = plt.figure()
    plt.plot(x,y)
    plt.xlabel('Threshold')
    plt.ylabel('No. of shared parameters')
    plt.title('Effect of consensus threshold on shared parameters\n Detection threshold %f'%detection_threshold)
    if save_file:
        plt.savefig(save_file)
    plt.show()
    
"""

def important_families(samples, consensus_threshold, detection_threshold):
    reg_param = region_parameters(my_som, samples, variable_list, threshold=detection_threshold, only_bmus=True)
    class_descriptions = [x.split('\t')[-1].replace('\n','') for x in open('MergedData/class_description_r2.tsv','r').readlines()][1:]
    potential_nodes = [x for x in reg_param if reg_param[x] >= consensus_threshold] # Nodes that are present in more than 50% of the cells, note that the cells does not always corresponds 1:1 with the original sample
    print('Important nodes:\n')
    for p in potential_nodes:
        fID = int(p.split('_')[1][1:])
        print(fID, ':\t', class_descriptions[fID])
"""
