#install.packages('ctv')
library('ctv')  
#install.views('Phylogenetics')
#update.views('Phylogenetics')
library(ape)
library(phytools)


args <- commandArgs(trailingOnly = TRUE)
fileName <- args[[1]]
saveFile <- args[[2]]
thresh <- as.numeric(args[[3]])

MyTrees = read.newick(fileName)
  for (i in 1:length(MyTrees)){
    #dup = rep(MyTrees[i],5)
    MyTrees[[i]] <- collapse.singles(MyTrees[[i]])
    #MyTrees[[i]]$edge.length = rep(MyTrees[[i]]$edge.length,2)
    #MyTrees[[i]] <- collapse.singles(consensus(dup,p=1))
  }
  pdf(saveFile, width=5,height=10)
  cons = collapse.singles(consensus(MyTrees,p=thresh))
  
  plotTree(cons,main='Consensus Tree',xlab='Thresh',fsize=1.2)#,type='clado')
  total_props = prop.part(MyTrees)
  consensus_props = prop.part(cons)
  c=0
  for (p in consensus_props){
    c = c + 1
    for (i in 1:length(total_props)){
      if (identical(total_props[[i]],p)){
        pct = attributes(total_props)$number[i]/length(MyTrees)
        nodelabels(round(pct,1),length(cons$tip.label)+c,cex=1)
      }
    }
  }
  
  invisible(dev.off())
